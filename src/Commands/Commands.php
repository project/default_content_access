<?php

namespace Drupal\default_content_access\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\default_content\Commands\DefaultContentCommands;
use Drupal\default_content\ExporterInterface;
use Drupal\default_content\ImporterInterface;

/**
 * Provides Drush commands for the default_content_access module.
 *
 * @package Drupal\default_content_access
 */
class Commands extends DefaultContentCommands {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Commands constructor.
   *
   * @param \Drupal\default_content\ExporterInterface $default_content_exporter
   *   The default content exporter.
   * @param \Drupal\default_content\ImporterInterface $default_content_importer
   *   The default content importer.
   * @param array[] $installed_modules
   *   Installed modules list from the 'container.modules' container parameter.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(ExporterInterface $default_content_exporter, ImporterInterface $default_content_importer, array $installed_modules, ModuleHandlerInterface $module_handler, FileSystemInterface $file_system, Connection $database) {
    parent::__construct($default_content_exporter, $default_content_importer, $installed_modules);
    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;
    $this->database = $database;
  }

  /**
   * Exports all the content access setting defined in a module info file.
   *
   * @param string $module
   *   The name of the module.
   *
   * @command default-content-access:export-module
   * @aliases dcaem
   *
   * @throws \JsonException
   */
  public function contentExportModule($module): void {
    if (!in_array($module, $this->installedExtensions, TRUE)) {
      return;
    }
    parent::contentExportModule($module);

    $serialized_by_type = $this->defaultContentExporter->exportModuleContent($module);
    $module_folder = $this->moduleHandler
      ->getModule($module)
      ->getPath() . '/access';
    $this->fileSystem->prepareDirectory($module_folder, FileSystemInterface::CREATE_DIRECTORY);

    $file_name = $module_folder . DIRECTORY_SEPARATOR . 'node.json';
    if (file_exists($file_name)) {
      $this->fileSystem->delete($file_name);
    }
    if (!empty($serialized_by_type['node'])) {
      $output = [];
      foreach ($serialized_by_type['node'] as $uuid => $value) {
        $query = $this->database->select('content_access', 'ca');
        $query->join('node', 'n', 'ca.nid = n.nid');
        $settings = $query->fields('ca', ['settings'])
          ->condition('n.uuid', $uuid)
          ->execute()
          ->fetchField();
        if (!empty($settings)) {
          $output[$uuid] = $settings;
        }
      }
      file_put_contents($file_name, json_encode($output, JSON_THROW_ON_ERROR));
    }
  }

  /**
   * Imports all the content access defined in a module info file.
   *
   * @param string $module
   *   The name of the module.
   * @param array $options
   *   The options.
   *
   * @command default-content-access:import-module
   * @option update-existing Flag if existing content should be updated,
   *   defaults to FALSE.
   * @aliases dcaim
   *
   * @throws \JsonException
   */
  public function contentImportModule(string $module, array $options = ['update-existing' => FALSE]): void {
    if (!in_array($module, $this->installedExtensions, TRUE)) {
      return;
    }
    if (!method_exists(DefaultContentCommands::class, 'contentImportModule')) {
      // @todo This has not landed in the official code yet.
      // @see https://www.drupal.org/project/default_content/issues/2640734
      return;
    }
    parent::contentImportModule($module, $options);

    $serialized_by_type = $this->defaultContentExporter->exportModuleContent($module);
    $module_folder = $this->moduleHandler
      ->getModule($module)
      ->getPath() . '/access';
    $file_name = $module_folder . DIRECTORY_SEPARATOR . 'node.json';
    if (!file_exists($file_name)) {
      return;
    }
    $settings = json_decode(file_get_contents($file_name), TRUE, 512, JSON_THROW_ON_ERROR);
    $nids = $this->database->select('node', 'n')
      ->fields('n', ['uuid', 'nid'])
      ->condition('n.uuid', array_keys($serialized_by_type['node']), 'IN')
      ->execute()
      ->fetchAllKeyed();
    $this->database->delete('content_access')
      ->condition('nid', array_values($nids), 'IN')
      ->execute();
    $query = $this->database->insert('content_access')
      ->fields(['nid', 'settings']);
    foreach ($settings as $uuid => $setting) {
      $query->values([$nids[$uuid], $setting]);
    }
    try {
      $query->execute();
    }
    catch (\Exception $e) {
      // @todo Log this exception.
    }
    node_access_rebuild();
  }

}
