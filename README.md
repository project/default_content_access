Default Content Access

INTRODUCTION
------------

This file extends the modules Default Content and Content Access such that for
exporting and importing nodes by the default content module will also export
and import their content access settings if available.

REQUIREMENTS
------------

This module requires the following modules:
 * Default Content (https://www.drupal.org/project/default_content)
 * Content Access (https://www.drupal.org/project/content_access)

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
For further information, visit:
 https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
When enabled, the module will also export and import their content access
settings if available along with exporting and importing nodes.
